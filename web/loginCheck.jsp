<%@ page import="myapp.bo.LookUsers" %><%--
  Created by IntelliJ IDEA.
  User: Kung
  Date: 2016-09-27
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%
        String username=request.getParameter("uname");
        String password=request.getParameter("pass");

        if(LookUsers.checkUser(username,password)){
            session.setAttribute("login",null);
            session.setAttribute("username",username);
            session.setAttribute("pass",password);
            response.sendRedirect("shop.jsp");
        }
        else{
            session.setAttribute("login","fail");
            response.sendRedirect("login.jsp");
        }
    %>

</body>
</html>
