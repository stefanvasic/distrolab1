<%@ page import="myapp.bo.LookUsers" %>
<%@ page import="myapp.bo.LookOrder" %><%--
  Created by IntelliJ IDEA.
  User: Kung
  Date: 2016-09-30
  Time: 14:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    String remove = request.getParameter("remove from cart");
    String order = request.getParameter("order");
    Object userid = session.getAttribute("username");
    String userId = (String) userid;
    Object password = session.getAttribute("pass");
    String pass = (String) password;

    if(remove!=null){
        LookUsers.removeFromUsersCart(userId,Integer.parseInt(remove));
        response.sendRedirect("cart.jsp");
    }

    else if(order!=null){
        LookOrder.createOrder(userId,pass);
        response.sendRedirect("order.jsp");
    }
%>
</body>
</html>
