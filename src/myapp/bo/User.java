package myapp.bo;

import myapp.db.UserDB;

/**
 * Created by Kung on 2016-09-27.
 */
public class User {
    private String username;
    private String password;

    protected User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    static public User searchUser(String username) {
        return UserDB.searchUserByUsername(username); }

    static public boolean addUser(String username, String password){
       UserDB.addUser(username, password);
        return true;
    }

    static public void addToCart(String username,int id){
        Cart.addToCart(username,id);
    }

    static public void removeFromCart(String username,int id){
        Cart.removeFromCart(username,id);
    }

    public String getPassword(){
        return password;
    }

    public String getUsername(){
        return username;
    }


}
