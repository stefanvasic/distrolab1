package myapp.bo;

import myapp.db.OrderDB;

import java.util.Date;

/**
 * Created by Kung on 2016-10-01.
 */
public class Order {
    private User user;
    private Date date;
    private Cart cart;

    protected Order(User user, Date date, Cart cart){
        this.user = user;
        this.date = date;
        this.cart = cart;
    }

    static public void addOrder(Order order){
        OrderDB.addOrder(order);
    }


    public Date getDate(){
        return date;
    }
    public User getUser() { return user;}
    public Cart getCart() { return cart; }
}
