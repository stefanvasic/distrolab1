package myapp.bo;

/**
 * Created by stefanvasic on 2016-09-26.
 */

import java.util.*;

public class LookItems {
    public static ArrayList<ItemInfo> getItems() {
        Collection c = Item.searchItems();
        ArrayList<ItemInfo> itemList = new ArrayList<>();
        Iterator it = c.iterator();



        for(int i=0; i<c.size();i++){
            Item x = (Item) it.next();
            int id= x.getId();
            String name = x.getName();
            int price = x.getPrice();
            itemList.add(new ItemInfo(name,price,id));

        }

        return itemList;
    }
}
