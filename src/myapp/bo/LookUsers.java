package myapp.bo;



/**
 * Created by Kung on 2016-09-27.
 */
public class LookUsers {
    public static boolean checkUser(String username, String password) {
        User user = User.searchUser(username);

        if(user!=null){
            if(user.getUsername().equals(username) && user.getPassword().equals(password))
                return true;
        }
        return false;
    }

    public static boolean addUser(String username, String password){
       if(User.searchUser(username) == null){ //no user with the same username exists
           User.addUser(username, password);
           return true;
       }
        else{
           return false;
       }

    }

    public static void addToUsersCart(String user,int itemId){
        User.addToCart(user,itemId);
    }

    public static void removeFromUsersCart(String user,int itemId){
        User.removeFromCart(user,itemId);
    }
}
