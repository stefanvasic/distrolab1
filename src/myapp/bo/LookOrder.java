package myapp.bo;

import java.util.Date;

/**
 * Created by Kung on 2016-10-01.
 */
public class LookOrder {
    public static void createOrder(String username, String password){
        User user = new User(username, password);
        Cart cart = Cart.getItems(username);
        Order order = new Order(user,new Date(), cart);
        order.addOrder(order);
    }
}
