package myapp.bo;

/**
 * Created by stefanvasic on 2016-10-03.
 */
public class ItemInfo {
    private String name;
    private int price;
    private int id;

    public ItemInfo(String name, int price, int id){
        this.name = name;
        this.price=price;
        this.id = id;
    }
    public String getName() {
        return name; }

    public int getPrice() {
        return price; }
    public int getId() {
        return id; }
}
