package myapp.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * Created by Kung on 2016-09-28.
 */
public class LookCart {
    public static ArrayList<ItemInfo> getItemsInCart(String user) {
        Cart cart = Cart.getItems(user);
        Collection c = cart.getCollection();
        ArrayList<ItemInfo> itemList= new ArrayList<>();

        Iterator it = c.iterator();
        for(int i=0; i<c.size();i++){
            Item x = (Item) it.next();
            int id= x.getId();
            String name = x.getName();
            int price = x.getPrice();
            itemList.add(new ItemInfo(name,price,id));

        }
        return itemList;
    }
}
