package myapp.bo;

/**
 * Created by stefanvasic on 2016-09-26.
 */
import myapp.db.ItemDB;

import java.util.Collection;

public class Item {
    private int id;
    private String name;
    private int price;
    static public Collection searchItems() {
        return ItemDB.searchItems(); }
    protected Item(int id, String name, int price) { this.id = id;
        this.name = name;
        this.price = price;
    }
    public String getName() {
        return name; }

    public int getPrice() {
        return price; }

    public int getId(){
        return id;
    }
}