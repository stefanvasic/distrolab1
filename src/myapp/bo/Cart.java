package myapp.bo;

import myapp.db.CartDB;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

/**
 * Created by Kung on 2016-09-28.
 */
public class Cart {
    private Vector items;

    protected Cart (){
        items=new Vector();
    }

    static public void addToCart(String username,int id){
        CartDB.addItemToUser(username,id);
    }

    static public void  removeFromCart(String username,int id){
        CartDB.removeItemFromUser(username,id);
    }

    static public Cart getItems(String username) {
        return CartDB.getItems(username);
    }

    public void add(int id,String name,int price){
        items.add(new Item(id,name,price));
    }


    public Collection getCollection(){
        return items;
    }

}
