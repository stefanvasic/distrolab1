package myapp.db;

/**
 * Created by stefanvasic on 2016-09-26.
 */
import myapp.bo.Item;

import java.util.*;
import java.sql.*;

public class ItemDB  extends Item {

    public static Collection searchItems() {
        Vector v = new Vector();
        Connection con = DBManager.getConnection();
        try{
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select id, itemname, itemprice from T_item ");
            while (rs.next()) {
                int i = rs.getInt("id");
                String name = rs.getString("itemname");
                int price = rs.getInt("itemprice");
                v.addElement(new ItemDB(i, name, price));
            }
            con.close();
            st.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            if(con!=null){
                try{
                    con.close();
                }catch (Exception ex){

                }
            }
        }

        return v;
    }

    private ItemDB(int id, String name, int price) {
        super(id, name, price);
    }
}
