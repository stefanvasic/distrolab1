package myapp.db;

import java.sql.Connection;
import javax.naming.InitialContext;
import javax.sql.DataSource;
/**
 * Created by stefanvasic on 2016-09-26.
 */
public class DBManager {
    static boolean isPoolInitiated=false;
    static Connection conn=null;
    static DataSource ds;


    public static synchronized Connection getConnection() {
        try {
            if (!isPoolInitiated) {
                InitialContext ctx = new InitialContext();

                ds =
                        (DataSource) ctx.lookup("java:comp/env/jdbc/MySQLDB");
                isPoolInitiated = true;
                System.out.println("Initiating pool");
            }
            conn = ds.getConnection();
            return conn;

        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return null;
    }
}
