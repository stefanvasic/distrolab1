package myapp.db;

import myapp.bo.Cart;
import myapp.bo.Item;
import myapp.bo.Order;
import myapp.bo.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by Kung on 2016-10-01.
 */
public class OrderDB extends Order {
    private OrderDB(User user, Date date, Cart cart) {
        super(user, date,cart);
    }

    public static void addOrder(Order order){
        Connection con = DBManager.getConnection();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(order.getDate());

        try{
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT id FROM t_user WHERE username= '" + order.getUser().getUsername() + "'");
            int userID=0;
            while(rs.next()){
                userID = rs.getInt("id");
            }

            Collection c  = order.getCart().getCollection();

            con.setAutoCommit(false);
            Iterator it = c.iterator();
            int previous=-1;
            for(int i=0; i<c.size();i++){
                Item x = (Item) it.next();
                if(previous!=x.getId()){
                    st.executeUpdate("INSERT INTO T_order (orderDate, userID, itemID, quantity)" +
                            "VALUES ('" + currentTime + "', '" + userID + "' , '" +  x.getId() + "', '1' )");
                    previous=x.getId();
                }
                else{
                    st.executeUpdate("UPDATE T_order SET quantity = quantity + 1 WHERE itemID = '" + x.getId() + "'");
                }
                st.executeUpdate("DELETE FROM T_cart WHERE userID = '" + userID + "' AND itemID = '" + x.getId() + "' LIMIT 1");
            }

            con.commit();
            con.setAutoCommit(true);

            con.close();
            st.close();


        }catch (SQLException ex){
            ex.printStackTrace();

        }finally {
            if(con!=null){
                try{
                    con.close();
                }catch (Exception ex){

                }
            }
        }
    }
}
