package myapp.db;

import myapp.bo.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Kung on 2016-09-27.
 */
public class UserDB extends User {

    private UserDB(String username, String password) {
        super(username, password);
    }

    public static User searchUserByUsername(String username){
        Connection con = DBManager.getConnection();
        UserDB user = null;
        try{
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select username, password from T_user WHERE username = '" + username+ "'");
            while (rs.next()) {
                String name = rs.getString("username");
                String password = rs.getString("password");
                user = new UserDB(name,password);
            }
            con.close();
            st.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if(con!=null){
                try{
                    con.close();
                }catch (SQLException sqlEx){

                }
            }
        }
        return user;

    }

    public static boolean addUser(String username, String password){
        Connection con = DBManager.getConnection();
        try{
            Statement st = con.createStatement();
            String s = "INSERT INTO T_user(username, password) " +
                    "VALUES ('" + username + "', '" + password + "')";
            st.executeUpdate(s);
            return true;

        }catch (Exception ex){
            ex.printStackTrace();
            return true;
        }
    }

}
