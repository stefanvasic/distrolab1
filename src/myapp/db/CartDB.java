package myapp.db;

import myapp.bo.Cart;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Created by Kung on 2016-09-28.
 */
public class CartDB extends Cart {
    private CartDB() {
        super();

    }
    public static void addItemToUser(String username,int itemId){
            Connection con = DBManager.getConnection();
            try{
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("select id from T_user WHERE username = '" + username+ "'");

                int userID=0;
                while (rs.next()) {
                    userID = rs.getInt("id");
                }
                if(userID!=0){
                    String s = "INSERT INTO T_cart(userID, itemID) " +
                            "VALUES ('" + userID + "', '" + itemId + "')";
                    st.executeUpdate(s);
                }
                con.close();
                st.close();

            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if(con!=null){
                    try{
                        con.close();
                    }catch (SQLException sqlEx){

                    }
                }
            }
    }

    public static CartDB getItems(String username){
        Connection con = DBManager.getConnection();
        CartDB cartDB = new CartDB();
        try{
            Statement st = con.createStatement();

            ResultSet rs = st.executeQuery("select T_item.id, T_item.itemname, T_item.itemprice from T_item" +
                        " INNER JOIN T_cart ON T_item.id = T_cart.itemID INNER JOIN T_user ON T_cart.userID = T_user.id" +
                        " WHERE T_user.username = '" + username + "' ORDER BY id");


            while (rs.next()) {
                int i = rs.getInt("id");
                String name = rs.getString("itemname");
                int price = rs.getInt("itemprice");
                cartDB.add(i, name, price);
            }
            con.close();
            st.close();
            return cartDB;


        }catch (Exception ex){

        }finally {
            if(con!=null){
                try{
                    con.close();
                }catch (SQLException sqlEx){

                }
            }
        }
        return cartDB;

    }

    public static void removeItemFromUser(String username,int id){
        Connection con = DBManager.getConnection();
        try{
            Statement st = con.createStatement();
            st.executeUpdate("DELETE FROM T_cart WHERE T_cart.itemID = '" + id
                    + "' AND T_cart.userID = (SELECT id FROM T_user WHERE username = '" + username + "') LIMIT 1");

            con.close();
            st.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if(con!=null){
                try{
                    con.close();
                }catch (SQLException sqlEx){

                }
            }
        }
    }
}
